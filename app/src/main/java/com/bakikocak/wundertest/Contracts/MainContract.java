package com.bakikocak.wundertest.Contracts;

import com.bakikocak.wundertest.Model.Car;
import java.util.List;

public class MainContract {

    public interface View {

        void showProgressBar(boolean isVisible);
        void onCarsDataRetrieved(List<Car> carList);
        void setResultText(int length);
        void onObservableError(Throwable error);
    }

    public interface Presenter {
        void createPlacemarksRequest();
    }
}
