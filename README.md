# README #


### What is this repository for? ###

This repository contains a sample that displays all the cars information & their location on the map using Wunder's cars location API provided in the test.

### A few points about the app ###

The app is written in Java and basically made of two activities entitled MainActivity and MapActivity and each activity has its own Presenter class as the app has a MVP pattern. (MainActivity -> MainPresenter, MapActivity -> MapPresenter etc.)

MainActivity lists all the car info retrieved from the API within a RecyclerView in which each row is a CardView.

When user selects a car on the list, MapActivity will be launched displaying that car in the center of the map. User can also select other cars by tapping them on the map and this way the info (name & address) of the car will be displayed on the top of the map pin.

### Tech Details ###

To be able to create a list with RecyclerView, Android's support library's dependency is included.
Fetching data is conducted by using Retrofit2 & RxJava and for modelling gson is used.
To be able to display cars' location on the map, Google Play Services' dependency is also included.
