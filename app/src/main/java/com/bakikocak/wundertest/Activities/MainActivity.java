package com.bakikocak.wundertest.Activities;

import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bakikocak.wundertest.CarAdapter;
import com.bakikocak.wundertest.Contracts.MainContract;
import com.bakikocak.wundertest.Model.Car;
import com.bakikocak.wundertest.Presenters.MainPresenter;
import com.bakikocak.wundertest.R;
import com.bakikocak.wundertest.Utils;
import java.util.List;


public class MainActivity extends AppCompatActivity implements MainContract.View {

    private static final String TAG = MainActivity.class.getSimpleName();

    private MainContract.Presenter mPresenter;
    private LinearLayoutManager mLayoutManager;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    private TextView errorTextView, resultTextView;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.tb_main_toolbar);
        mRecyclerView = findViewById(R.id.rv_main);
        resultTextView = findViewById(R.id.tv_result_text);
        errorTextView = findViewById(R.id.tv_error_text);
        progressBar = findViewById(R.id.pb_main_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        setupToolbar();
        setupRecyclerView();

        mPresenter = new MainPresenter(this);
        isLoading = true;
        mPresenter.createPlacemarksRequest();
    }

    private void setupToolbar() {
       // mToolbar.setTitle(R.string.app_name);
        setSupportActionBar(mToolbar);
    }

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void showProgressBar(boolean isVisible) {
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCarsDataRetrieved(List<Car> carList) {
        isLoading = false;
        addItemsToList(carList);
    }

    @Override
    public void setResultText(int length) {
        resultTextView.setText(String.format(
                getString(R.string.list_results_text),
                length
        ));
    }

    private void addItemsToList(List<Car> carList) {
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(new CarAdapter(MainActivity.this, carList));
        }

        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onObservableError(Throwable error) {

        if (!Utils.isNetworkAvailable(this) && (mRecyclerView.getAdapter() == null || mRecyclerView.getAdapter().getItemCount() == 0)) {
            showProgressBar(false);
            showErrorMessage(getString(R.string.error_network));
        }
        Log.e(TAG, "An ObservableError occurred: ", error);
        isLoading = false;
    }

    private void showErrorMessage(String message) {

        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(message);
    }
}
