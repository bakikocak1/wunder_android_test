package com.bakikocak.wundertest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bakikocak.wundertest.Activities.MapActivity;
import com.bakikocak.wundertest.Model.Car;
import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private static final String TAG = CarAdapter.class.getSimpleName();

    private Context context;
    private List<Car> items;


    public CarAdapter(Context context, List<Car> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Car singleCarItem = items.get(position);

        holder.carName.setText(singleCarItem.getName());
        holder.carAddress.setText(singleCarItem.getAddress());
        holder.carVin.setText(singleCarItem.getVin());

        holder.interiorText.setText(context.getString(R.string.interior_title) + ": " + singleCarItem.getInterior());
        holder.exteriorText.setText(context.getString(R.string.exterior_title) + ": " + singleCarItem.getExterior());
        holder.engineType.setText(context.getString(R.string.engine_type_title) + ": " + singleCarItem.getEngineType());

        holder.parentLayout.setOnClickListener(view -> {
            Intent intent = new Intent(context, MapActivity.class);
            intent.putExtra("selectedCar",singleCarItem);
            context.startActivity(intent);
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout parentLayout;
        private TextView carName;
        private TextView carAddress;
        private TextView carVin;

        private TextView interiorText;
        private TextView exteriorText;
        private TextView engineType;

        public ViewHolder(View itemView) {
            super(itemView);

            parentLayout = itemView.findViewById(R.id.rl_parent_layout);
            carName = itemView.findViewById(R.id.tv_car_name);
            carAddress = itemView.findViewById(R.id.tv_car_address);
            carVin = itemView.findViewById(R.id.tv_car_vin);

            interiorText = itemView.findViewById(R.id.tv_interior_text);
            exteriorText = itemView.findViewById(R.id.tv_exterior_text);
            engineType = itemView.findViewById(R.id.tv_engine_type);
        }
    }

}

