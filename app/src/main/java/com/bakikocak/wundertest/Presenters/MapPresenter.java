package com.bakikocak.wundertest.Presenters;

import android.util.Log;

import com.bakikocak.wundertest.Api.WunderRetrofitHelper;
import com.bakikocak.wundertest.Contracts.MapContract;
import com.bakikocak.wundertest.Model.Car;
import com.bakikocak.wundertest.Model.Placemarks;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MapPresenter implements MapContract.Presenter {

    public static final String TAG = MapPresenter.class.getSimpleName();
    private MapContract.View mMapActivity;

    public MapPresenter(MapContract.View mMapActivity) {
        this.mMapActivity = mMapActivity;
    }

    @Override
    public void createPlacemarksRequest() {
        mMapActivity.showProgressBar(true);
        WunderRetrofitHelper.getCreatePlacemarksRequestObservable()
                .subscribe(new Observer<Placemarks>() {
                               @Override
                               public void onSubscribe(Disposable d) {
                               }

                               @Override
                               public void onNext(Placemarks placemarks) {
                                   List<Car> carsList = placemarks.getPlacemarks();
                                   mMapActivity.onCoordinatesDataRetrieved(carsList);
                                   mMapActivity.showProgressBar(false);
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mMapActivity.onObservableError(e);
                               }

                               @Override
                               public void onComplete() {
                                   Log.d(TAG, "onComplete: ");
                               }
                           }
                );
    }
}

