package com.bakikocak.wundertest.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bakikocak.wundertest.Contracts.MapContract;
import com.bakikocak.wundertest.Model.Car;
import com.bakikocak.wundertest.Presenters.MapPresenter;
import com.bakikocak.wundertest.R;
import com.bakikocak.wundertest.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;


public class MapActivity extends AppCompatActivity implements MapContract.View, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final String TAG = MapActivity.class.getSimpleName();

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 9999;
    private static final float DEFAULT_ZOOM = 15f;

    private Car selectedCar;
    private List<Car> carList;
    private ArrayList<Marker> markerList = new ArrayList<Marker>();
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Boolean mLocationPermissionsGranted = false;
    private FrameLayout mapContainerLayout;

    private MapPresenter mPresenter;
    private TextView errorTextView;
    private ProgressBar progressBar;
    private boolean isLoading = false;
    private boolean isMarkerToggled = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Intent intent = getIntent();
        selectedCar = intent.getParcelableExtra("selectedCar");

        mapContainerLayout = findViewById(R.id.fl_map_container);
        errorTextView = findViewById(R.id.tv_error_text);
        progressBar = findViewById(R.id.pb_map_progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        mPresenter = new MapPresenter(this);
        isLoading = true;
        //I preferred fetching locations again in case that car data is changed while switching to MapActivity from MainActivity in a more complex app.
        mPresenter.createPlacemarksRequest();
    }

    @Override
    public void onCoordinatesDataRetrieved(List<Car> list) {
        getLocationPermission();
        carList = list;
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                initializeMap();
            } else {
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void initializeMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapActivity.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mLocationPermissionsGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            for (int i = 0; i < carList.size(); i++) {
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car))
                        .position(new LatLng((carList.get(i).getCoordinates().get(1)), (carList.get(i).getCoordinates().get(0))))
                        .snippet(carList.get(i).getAddress())
                        .title(carList.get(i).getName()));

                markerList.add(marker);

                // Firstly, we will focus on the car selected on the list. Selected car will be shown in the center of the screen.
                // More info about the car could be displayed by tabbing on the icon.
                if (selectedCar.getName().equals(carList.get(i).getName())) {
                    moveCamera(new LatLng((selectedCar.getCoordinates().get(1)), (selectedCar.getCoordinates().get(0))), DEFAULT_ZOOM);
                }
            }
        }

        mMap.setOnMarkerClickListener(this);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(MapActivity.this, "clicked map", Toast.LENGTH_SHORT).show();
                showAllMarkers();
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        // Toggle the marker that user selected and hide/show others.
        if (!isMarkerToggled) {
            hideOtherMarkers(marker);
            isMarkerToggled = true;
        } else {
            showAllMarkers();
            marker.hideInfoWindow();
            return true;
        }
        return false;
    }

    private void showAllMarkers() {
        for (int i = 0; i < markerList.size(); i++) {
            markerList.get(i).setVisible(true);
        }
        isMarkerToggled = false;
    }

    private void hideOtherMarkers(Marker marker) {
        for (int i = 0; i < markerList.size(); i++) {
            if (!markerList.get(i).equals(marker)) {
                markerList.get(i).setVisible(false);
            }
        }
    }

    private void getDeviceLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Location found also camera can be moved to the current location if needed!");
                            Location currentLocation = (Location) task.getResult();
                            // moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);
                        } else {
                            Toast.makeText(MapActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: Permission denied");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: Permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize map
                    initializeMap();
                }
            }
        }
    }

    @Override
    public void showProgressBar(boolean isVisible) {
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mapContainerLayout.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onObservableError(Throwable error) {
        if (!Utils.isNetworkAvailable(this)) {
            showProgressBar(false);
            showErrorMessage(getString(R.string.error_network));
        }
        Log.e(TAG, "An ObservableError occurred: ", error);
        isLoading = false;
    }

    private void showErrorMessage(String message) {
        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(message);
    }

}


