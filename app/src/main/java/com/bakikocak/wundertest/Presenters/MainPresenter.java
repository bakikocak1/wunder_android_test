package com.bakikocak.wundertest.Presenters;

import android.util.Log;
import com.bakikocak.wundertest.Api.WunderRetrofitHelper;
import com.bakikocak.wundertest.Contracts.MainContract;
import com.bakikocak.wundertest.Model.Car;
import com.bakikocak.wundertest.Model.Placemarks;
import java.util.List;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainPresenter implements MainContract.Presenter {

    public static final String TAG = MainPresenter.class.getSimpleName();
    private MainContract.View mMainActivity;

    public MainPresenter(MainContract.View mMainActivity) {
        this.mMainActivity = mMainActivity;
    }

    @Override
    public void createPlacemarksRequest() {
        mMainActivity.showProgressBar(true);

        WunderRetrofitHelper.getCreatePlacemarksRequestObservable()
                .subscribe(new Observer<Placemarks>() {
                               @Override
                               public void onSubscribe(Disposable d) {
                               }

                               @Override
                               public void onNext(Placemarks placemarks) {
                                   List<Car> carsList = placemarks.getPlacemarks();
                                   mMainActivity.onCarsDataRetrieved(carsList);
                                   mMainActivity.setResultText(carsList.size());
                                   mMainActivity.showProgressBar(false);
                               }

                               @Override
                               public void onError(Throwable e) {
                                   mMainActivity.onObservableError(e);
                               }

                               @Override
                               public void onComplete() {
                                   Log.d(TAG, "onComplete: ");
                               }
                           }
                );
    }
}
