package com.bakikocak.wundertest.Contracts;

import com.bakikocak.wundertest.Model.Car;

import java.util.List;

public class MapContract {

    public interface View {

        void showProgressBar(boolean isVisible);
        void onCoordinatesDataRetrieved(List<Car> carList);
        void onObservableError(Throwable error);
    }

    public interface Presenter {
        void createPlacemarksRequest();
    }
}
