package com.bakikocak.wundertest.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Placemarks {

    @SerializedName("placemarks")
    @Expose
    private List<Car> placemarks = null;

    public List<Car> getPlacemarks() {
        return placemarks;
    }

    public void setPlacemarks(List<Car> placemarks) {
        this.placemarks = placemarks;
    }
}
