package com.bakikocak.wundertest.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Car implements Parcelable{

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("coordinates")
    @Expose
    private ArrayList coordinates = null;
    @SerializedName("engineType")
    @Expose
    private String engineType;
    @SerializedName("exterior")
    @Expose
    private String exterior;
    @SerializedName("fuel")
    @Expose
    private Integer fuel;
    @SerializedName("interior")
    @Expose
    private String interior;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("vin")
    @Expose
    private String vin;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    protected Car(Parcel in) {
        address = in.readString();
        engineType = in.readString();
        exterior = in.readString();
        if (in.readByte() == 0) {
            fuel = null;
        } else {
            fuel = in.readInt();
        }
        coordinates = in.readArrayList(Car.class.getClassLoader());
        interior = in.readString();
        name = in.readString();
        vin = in.readString();
    }

    public static final Creator<Car> CREATOR = new Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel in) {
            return new Car(in);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(address);
        parcel.writeString(engineType);
        parcel.writeString(exterior);
        if (fuel == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(fuel);
        }
        parcel.writeList(coordinates);
        parcel.writeString(interior);
        parcel.writeString(name);
        parcel.writeString(vin);
    }
}