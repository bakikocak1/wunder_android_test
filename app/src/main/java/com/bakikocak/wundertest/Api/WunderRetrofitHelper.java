package com.bakikocak.wundertest.Api;


import android.util.Log;
import com.bakikocak.wundertest.Model.Placemarks;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WunderRetrofitHelper {

    private static Retrofit retrofit = null;
    private static final String TAG = WunderRetrofitHelper.class.getSimpleName();

    public static Retrofit getBaseRetrofit(String baseUrl) {
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }


    public static Observable<Placemarks> getCreatePlacemarksRequestObservable() {
        final WunderApiService service = WunderRetrofitHelper
                .getBaseRetrofit(WunderApiService.BASE_URL)
                .create(WunderApiService.class);

        // retryWhen enables to request if there is an unexpected network error.
        // To test this, you can make an API call https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json/ which will return 403 error code to the client.
        // https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json works fine.
        return service.getPlacemarksObservable()
                .retryWhen(errorsObservable -> errorsObservable.flatMap(error -> {
                    if (error instanceof HttpException) {
                        Log.e(TAG, "getPlacemarksObservable: HttpException", error);
                        return errorsObservable.delay(1, TimeUnit.SECONDS);
                    }
                    return Observable.error(error);
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
