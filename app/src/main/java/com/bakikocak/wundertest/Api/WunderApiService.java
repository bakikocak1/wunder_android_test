package com.bakikocak.wundertest.Api;

import com.bakikocak.wundertest.Model.Placemarks;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface WunderApiService {
    String BASE_URL = "https://s3-us-west-2.amazonaws.com/wunderbucket/";

    @Headers("Accept: application/json")
    @GET("locations.json")
    Observable<Placemarks> getPlacemarksObservable();
}

